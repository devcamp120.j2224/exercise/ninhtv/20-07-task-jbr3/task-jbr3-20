package com.devcamp.split_string_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SplitStringApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SplitStringApiApplication.class, args);
	}

}
