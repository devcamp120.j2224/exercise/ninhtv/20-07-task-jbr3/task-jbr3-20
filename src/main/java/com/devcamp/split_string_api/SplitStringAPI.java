package com.devcamp.split_string_api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class SplitStringAPI {
    
    @GetMapping("/split")
    public ArrayList<String> splitstringapi(){
        ArrayList<String> result = new ArrayList<String>();
        String str = "how to do in java provides java tutorials";
        String[] strArray = str.split("\\s");
        for (int i = 0; i < strArray.length; i++) {
            result.add(strArray[i]);
          }
        return result;  
    }
   
}
